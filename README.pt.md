# saifr-mats-controller

## Arquivo Launcher - saifr-mats-controller.sh

```
#!/bin/bash

cd /home/pi/

wget -q --spider https://bitbucket.org

if [ $? -eq 0 ]; then
        rm -rf saifr-mats-controller
        git clone git@bitbucket.org:ictp-saifr/saifr-mats-controller.git
fi

cd saifr-mats-controller/
python main.py
```

Esse arquivo deve ser colocado no rc.local.

> **ATENÇÃO** A chave ssh do usuário `ROOT` deve ser configurada no bitbucket.org para o `git clone` funcionar.

> **ATENÇÃO** O programa que controla a câmera só sera atualizado no boot caso o Raspberry Pi tenha conseguido se conectar com `https://bitbucket.org`


## Arquivo de configuração: config.json ##

```
#!json

{
  "general": {
    "interval": 0.5,
    "log_level": 20
  },
  "camera": {
    "ip": "127.0.0.1",
    "port": 23,
    "credentials": {
      "username": "admin",
      "password": "password"
    }
  },
  "triggers": {
    "5":"camera preset recall 1",
    "7":"camera preset recall 1",
    "13":"camera preset recall 2",
    "21":"camera preset recall 2"    
  }
}
```

## general ##

Define o intervalo para verificação dos `triggers` e o nível de log.

## camera ##

Informações da câmera para realizar a conexão e a execução dos comandos associados nas triggers.

## triggers ##

A identificação das triggers ocorre no modo BCM do RaspberryPI. `gpio.setmode(gpio.BCM)`.
Para cada porta da trigger é associado um comando `telnet` que vai ser executado na `camera`.