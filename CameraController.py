import telnetlib
from telnetlib import IAC, NOP
from datetime import datetime

class VaddioCameraConnector:
  def __init__ (self, host, port, username, password, logger):
    self._host = host
    self._port = port
    self._username = username
    self._password = password
    self._timeout = 20
    self.__logger = logger
    self.telnetClient = None

  def __connect_to_camera(self):
    if self.__check_alive() == False:
      self.__write_log('Trying to connect to camera')
      try:
        self.telnetClient = telnetlib.Telnet(self._host, self._port, self._timeout)        
        self.__logger.info("Connected to camera '" + self._host + "'. Trying to log in.", 'VaddioCameraConnector')

        self.__logger.info(self.telnetClient.read_until("login:", 5))
        self.exec_telnet_command(self._username)

        self.__logger.info(self.telnetClient.read_until("Password:", 5))
        self.exec_telnet_command(self._password)

        self.__logger.info(self.telnetClient.read_until("Welcome"))
          
        self.__logger.info("Success", 'VaddioCameraConnector')
        return True
      except:
        self.__logger.error("Failed to connect to camera '" + self._host + "'", 'VaddioCameraConnector')
        raise

  def exec_telnet_command(self, command):
    self.__connect_to_camera()
    self.__logger.info("Executing command '" + command + "'", 'VaddioCameraConnector')
    self.telnetClient.write(command.encode("ascii") + b"\n")



  def __check_alive(self):
    try:
      if self.telnetClient == None:
        return False

      if self.telnetClient.sock: # this way I've taken care of problem if the .close() was called
        self.telnetClient.sock.send(IAC+NOP) # notice the use of send instead of sendall
        return True
      else:
        return False        
    except:
      raise

  def call_preset(self, preset_num):
    self.exec_telnet_command("camera preset recall "+str(preset_num)+"\n")
