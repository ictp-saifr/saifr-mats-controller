import sentry_sdk
sentry_sdk.init("https://cb752c876dcb4edbaf9e804a21b896a4@sentry.io/1417958")

import sys
import json
import logging
from logging.handlers import RotatingFileHandler
import time
import RPi.GPIO as gpio
import CameraController as CC

# LOGLEVEL
#CRITICAL = 50
#FATAL = CRITICAL
#ERROR = 40
#WARNING = 30
#WARN = WARNING
#INFO = 20
#DEBUG = 10
#NOTSET = 0

def setup_triggers(triggers):
  _logger.info("Setting up triggers")
  gpio.setmode(gpio.BCM)
  for (trigger, command) in triggers.items():
    _logger.info("Trigger '"+trigger+"' command '"+command+"'")
    gpio.setup(int(trigger), gpio.IN, pull_up_down = gpio.PUD_UP)
    gpio.add_event_detect(int(trigger), gpio.FALLING)

def setup_logger(level):
  _logger = logging.getLogger('MatsController')
  _logger.setLevel(level)
  formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s',
                                datefmt='%Y-%m-%d %H:%M:%S')
  handler = logging.handlers.RotatingFileHandler(
                'MatsController.log', maxBytes=1024*200, backupCount=5)
  handler.setFormatter(formatter)
  _logger.addHandler(handler)
  return _logger

with open('config.json', 'r') as f:
  config = json.load(f)

_logger = setup_logger(config['general']['log_level'])

interval = config['general']['interval']
camera_config = config['camera']
camera = CC.VaddioCameraConnector(
  camera_config['ip'],
  camera_config['port'],
  camera_config['credentials']['username'],
  camera_config['credentials']['password'],
  _logger
)

setup_triggers(config['triggers'])

while True:
  time.sleep(interval)
  try:
    for (trigger, command) in config['triggers'].items():
      if(gpio.event_detected(int(trigger))):
        _logger.info("Trigger '" + trigger + "' Activated. Trying to exec '" +  command + "'")
        camera.exec_telnet_command(command)
  except(KeyboardInterrupt):
    _logger.info('KeyboardInterrupt. Exiting...')
    break
  except:
    _logger.error("Uncaught exception. Waiting 2 seconds...", exc_info=True)
    time.sleep(2)

