# saifr-mats-controller

## Launcher - saifr-mats-controller.sh

```
#!/bin/bash

cd /home/pi/

wget -q --spider https://bitbucket.org

if [ $? -eq 0 ]; then
        rm -rf saifr-mats-controller
        git clone git@bitbucket.org:ictp-saifr/saifr-mats-controller.git
fi

cd saifr-mats-controller/
python main.py
```

This file must be configured on `rc.local`

> **INFO** The SSH key for `ROOT` user must be configured on bitbucket.org in order to allow `git clone` 

> **INFO** The auto-update will only run if it the Raspberry Pi has internet connection. This is checked trying to access `https://bitbucket.org`.


## config.json ##

```
#!json

{
  "general": {
    "interval": 0.5,
    "log_level": 20
  },
  "camera": {
    "ip": "127.0.0.1",
    "port": 23,
    "credentials": {
      "username": "admin",
      "password": "password"
    }
  },
  "triggers": {
    "5":"camera preset recall 1",
    "7":"camera preset recall 1",
    "13":"camera preset recall 2",
    "21":"camera preset recall 2"    
  }
}
```

## general ##

Define the interval of triggers and log level.

## camera ##

Camera connection information. by default Vaddio cameras has admin/password as credentials.

## triggers ##

The triggers (GPIO ports) are defined with BCM mode of Raspberry Pi.  `gpio.setmode(gpio.BCM)`.
For each trigger is associated the `telnet` command that will be run on the `camera`.